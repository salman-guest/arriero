This list doesn't pretend to be complete, it's sort of a roadmap, or a
pensieve for some bothering issues.

Fetch upstream and friends:

- Check Uscan, download current version for dfsg versions
- Check Uscan, dfsg rule, currently is hardcoded to +dfsg in the rest of the
  code
- Take into account Files-Excluded-<component> (debian/copyright)

Some packages don't have a watch file, and some might need a feature not
present in uscan (xawtv, for example), or upstream might not have any
releases (cinnamon-themes). Gracefully fail in these cases.

Gbp configuration:

- Per package configuration: fallback to the gbp configuration for the package
  upstream-branch and debian-branch.
- Fallback to the gbp configuration, when possible

Package cloning:

- !! Add the branches to sync in .git/config (in clone)
- ! Use debcheckout

- Download the current version instead of new version (if needed).

- Add autoconfigure to clone:
  + if there is an origin/upstream branch, upstream is pushed
  + if there is a origin/pristine-tar branch add pristine-tar
  + configure this in the .git/gbp.conf if not configured in the debian/gbp.conf

- If there's no basedir anywhere, we should use the current dir

- Clone needs to fetch the upstream branch
- Verify that the command used to clone exists

Processing packages:

- Check packages bugs on tests, check rc bugs not fixed.

- Add a configuration for a rule that generates the dfsg version
  + arriero uses prune-nonfree (it should be configurable)
  + uscan supports copyright format 1.0 exclude-files (need to test this with
    arriero)
  + gbp uses filters (filter-orig in arriero)

- Export build dir to work outside the git repo

- Allow to choose between pbuilder/cowbuilder/qemubuilder/debuild/sbuild
- Document the default setup requirements

- Clean up after overlay

Addons
------

* Allow having extra files that define extra actions.  These files should list
  the actions that they define, with a help string.  Arriero would then read
  from the files according to the configuration.

Status
------

* Add a status that indicates if the package needs to be uploaded or not (if
  it's not UNRELEASED), using rmadison:
  e.g. rmadison -a amd64 -s unstable cinnamon -u debian

Build
-----

* Add support for multi arch builds
* Avoid starting the shell twice when there is an error on the tests (shell on
  error)
* Should we start the shell twice when "shell" is requested and we run the
  tests?
* The hooks configuration is specific to git-pbuilder, it needs to be
  refactored.

Push
----

* ! This needs a serious refactor

Logs
----

- Parse build log and extract errors.
- Add a logcheck like functionality for builds and tests
- logs: report in a lintian style way
- Save command outputs to the log files

More actions
------------

- register based on mr register to add a package dir to the arriero
  configuration.

