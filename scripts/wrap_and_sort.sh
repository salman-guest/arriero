#!/bin/bash
# This script is intended to be run as:
# arriero exec -x wrap_and_sort.sh [packages]

set -e

wns_opt=''
ignore_new=''
while [ "$#" -ge 1 ]; do
    case "$1" in
        --wrap-and-sort-opt|-w)
            shift
            wns_opt="$1"
            shift
            ;;
        --ignore-new)
            shift
            ignore_new='yes'
            ;;
        *)
            break
            ;;
    esac
done

MSG='wrap-and-sort'

wrap-and-sort $wns_opt

if [ -n "$ignore_new" ]; then
    status_arg='--untracked-files=no'
fi

status=$(git status --porcelain $status_arg)

if [ -z "${status}" ]; then
    # No changes needed
    exit 0
fi

git commit -a -m "${MSG}"
