#!/bin/bash
# This script is intended to be run as:
# arriero exec -x "bump_debhelper.sh -m 'Commit message' -c $COMPAT" [packages]

RUN_DCH=1

compat=9
commit_message=''

while [ "$#" -ge 1 ]; do
    case "$1" in
        --dch)
            RUN_DCH=1
            shift
            ;;
        --no-dch)
            RUN_DCH=0
            shift
            ;;
        -m)
            shift
            commit_message="$1"
            shift
            ;;
        --compat|-c)
            shift
            compat="$1"
            shift
            ;;
        *)
            break
            ;;
    esac
done

if [ -z "$commit_message" ]; then
    commit_message='Bump debhelper build-dep and compat to '"$compat"'.'
fi

if [ $(cat debian/compat) -ge "$compat" ]; then
    # Nothing to do
    exit 0
fi

echo "$compat" > debian/compat

dhv=$(sed -n -r 's!.*\W(debhelper\s+\(>= ([^)~]+)\)).*!\2!p' debian/control)
if dpkg --compare-versions "$dhv" lt "$compat"; then
    sed -i -r 's!(\Wdebhelper\s+\(>=) [^)]+\)!\1 '"$compat"'~)!' \
        debian/control
fi

if [[ "$RUN_DCH" -ne 0 ]]; then
    dch "$commit_message"
fi

git commit -a -m "$commit_message"
