#!/bin/sh
# This script is intended to be run as:
# arriero exec -x "update_control.sh script" [packages]

wns_opt=''
ignore_new=''
while [ "$#" -ge 1 ]; do
    case "$1" in
        --wrap-and-sort-opt|-w)
            shift
            wns_opt="$1"
            shift
            ;;
        --ignore-new)
            shift
            ignore_new='yes'
            ;;
        *)
            break
            ;;
    esac
done

if [ $# -lt 1 ]; then
    echo "usage: $0 cmake_parser" > /dev/stderr
    exit 1
fi

script="$1"

"${@}"

if [ -n "$ignore_new" ]; then
    status_arg='--untracked-files=no'
fi

status=$(git status --porcelain $status_arg)
if [ -z "${status}" ]; then
    # No changes needed
    exit 0
fi

wrap-and-sort $wns_opt -f debian/control
git add debian/control
git commit -m 'Automatic debian/control update with '"$(basename "${script}")"

