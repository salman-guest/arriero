#!/bin/bash
# This script is intended to be run as:
# arriero exec -x 'bump_standards.sh [--no-dch] 3.9.6' [packages]

set -e

RUN_DCH=1

for i in "$@"; do
    case "$i" in
        --dch)
            RUN_DCH=1
            shift
            ;;
        --no-dch)
            RUN_DCH=0
            shift
            ;;
        *)
            break
            ;;
    esac
done

if [ $# -lt 1 ]; then
    echo "usage: $0 STANDARDS-VERSION" > /dev/stderr
    exit 1
fi

new_version="$1"
old_version=$(sed -n 's/^Standards-Version:\s*\(\S*\)/\1/p' debian/control)
newer=$(sort -rV <(echo ${new_version}) <(echo ${old_version}) | head -n1)

if [ "${newer}" = "${old_version}" ]; then
    # No changes needed
    exit 0
fi

sed -i 's/^Standards-Version:.*$/Standards-Version: '"${new_version}"'/' debian/control

if [[ "$RUN_DCH" -ne 0 ]]; then
    dch "Bump Standards-Version to ${new_version}."
fi

git commit -a -m "Bump Standards-Version to ${new_version}."
