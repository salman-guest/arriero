#!/bin/bash
# This script is intended to be run as:
# arriero exec -s upstream_diff.sh -f upstream_version,last_changelog_version [packages]

last_upstream=$(echo "$last_changelog_version" | sed -r 's/^.*?://;s/-.*?$//')

if [ -z "$upstream_version" -o -z "$last_upstream" ]; then
    # Nothing to compare
    exit 0
fi

old=$(echo "$last_upstream" | tr '~:' '_%')
new=$(echo "$upstream_version" | tr '~:' '_%')

if [ "$new" = "$old" ]; then
    # No version change
    exit 0
fi

echo '================================================================='
echo $PWD
echo git diff "upstream/${last_upstream}...upstream/${upstream_version}"
git diff "upstream/${old}...upstream/${new}"
