#!/bin/sh

REMOTE="origin"

LOCAL_TAGS=$(mktemp)
REMOTE_TAGS=$(mktemp)
cleanup () {
    rm -f "$LOCAL_TAGS" "$REMOTE_TAGS"
}
trap cleanup EXIT
eval "$(git for-each-ref 'refs/tags/debian/*' --sort 'version:refname' \
    --format 'if [ -n %(contents:signature) ]; then echo %(refname); fi' --shell)" > "$LOCAL_TAGS"
git ls-remote --tags "$REMOTE" 'debian/*' | awk '! /\^\{\}$/ {print $2}' | \
    sort -V > "$REMOTE_TAGS"

tags=$(comm -23 --nocheck-order "$LOCAL_TAGS" "$REMOTE_TAGS")
if [ -n "$tags" ]; then
    git push "$REMOTE" $tags
fi
