Version 0.7~20190830:
 * Migrate to python 3
 * Use the debian module whenever possible
 * Use the dependencies declared in the packages, no need to declare them in
   the configuration file.
 * Update setup.py
 * Run commands with pexpect
 * Add --download-version option to update
 * Add support for relative configs with += and -=
 * Add sign_upload option
 * Add name/suite support

Version 0.6.1:
 * Add source-only build option (-S)
 * Add dsc_file and source_changes_file field (for exec and list)
 * Add binary-only build option (-B)
 * Add arch-all/no-arch-all build option
 * Add update-symbols action
 * Add dist and package as field aliases
 * Select builder in a cli option
 * Add fields last_changelog_distribution last_changelog_version and
   version_at_distribution
 * Start external-uploads actions.
 * Add new script upstream_diff.sh
 * Add new script wrap_and_sort.sh
 * Add logdir option
 * Add (sbuild) hook list-missing to list not installed files
 * Add update_vcs_browser.sh script
 * Avoid calling create chroot when update fails
 * Add has_symbols field
 * Use the new gbp command names.
 * Handle debian/copyright excluded-files uscan mangle
 * Add a meta script for the control file
 * Add --run-autopkgtest={true|false} build option.
 * Add InternalDependencies action
 * Add a generic meta script (update.sh)
 * Use the current version by default in fetch_upstream
 * Push the upstream tags if the upstream branch is pushed

Version 0.6:
 * Make filter-orig a pattern list.
 * Allow the user to specify extra git-buildpackage options when building (-o).
 * Add feature to uscan class that allows to download arbitrary versions.
 * Ignore upstream releases when they have no changes.
 * Use downloaded tarballs, if present.
 * Create debian branch on pull, if missing.
 * Use current directory if no package name is received.
 * Several bugfixes regarding handling of configuration file
 * Added 'urgency' property for changelog, 'source_name' for package
 * Improve status output

Version 0.5:
 * Initial release.
