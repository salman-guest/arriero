# -*- coding: utf8 -*-
# Copyright: 2013-2014, Maximiliano Curia <maxy@debian.org>
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
'Interface to uscan'

import glob
import logging
import os
import re
import subprocess

from collections import defaultdict, namedtuple

import lxml.etree

import debian.debian_support as ds

from . import util
from .errors import UscanError

# Taken from gbp.deb.uscan and modified to suit this program
# Copyright: 2012, Guido Günther <agx@sigxcpu.org>
# License: GPL-2+

Component = namedtuple('Component', ['url', 'tarball'])


class Uscan(object):
    'Arriero uscan wrapper'

    cmd = '/usr/bin/uscan'

    class Package(object):
        'Minimal package representation'

        upstream_version = None
        # "Current" upstream version either taken from the debian/changelog or
        # from the --download-debversion option
        debian_upstream_version = None
        # debian_upstream_version after dropping the dfsg part
        debian_mangled_version = None

        def __init__(self, entry):
            self.components = set()

            self.package = entry['package']
            self.status = entry['status']
            self.upstream_version = entry['upstream-version']
            self.debian_upstream_version = entry['debian-uversion']
            self.debian_mangled_version = entry['debian-mangled-uversion']

            self.filename = None

        @property
        def version(self):
            'Latest upstream version'
            return self.upstream_version \
                if self.upstream_version else self.debian_upstream_version

        @property
        def url(self):
            'Urls for each uscan component'
            return set(component.url for component in self.components)

        @property
        def tarball(self):
            'Tarballs for each uscan component'
            return set(component.tarball for component in self.components)

        def add(self, url, tarball):
            'Add a new component'
            self.components.add(Component(url, tarball))

        def update(self, other):
            'Update components'
            self.components.update(other.components)

    def __init__(self, _dir='.', destdir='..'):
        self.uptodate = False
        self._result = None
        self._dir = os.path.abspath(_dir)
        self._destdir = destdir

    @property
    def tarball(self):
        'Tarball found if any'
        return self._result.tarball if self._result else None

    @property
    def version(self):
        'Version found if any'
        return self._result.version if self._result else None

    @property
    def upstream_version(self):
        'Upstream Version found if any'
        return self._result.upstream_version if self._result else None

    @property
    def url(self):
        'Url found if any'
        return self._result.url if self._result else None

    @property
    def components(self):
        'Components found if any'
        return self._result.components if self._result else None

    @staticmethod
    def _process_package_check_repacked(entry, package, filename):

        match = re.search(r'Successfully repacked (?:.+) as (.+),',
                          entry['messages'])
        if match:
            filename = match.group(1)
            # Use the repacked version
            match = re.match(r'(?:.*)_([^_]+)\.orig\.', filename)
            if match:
                package.upstream_version = match.group(1)
        return filename

    @staticmethod
    def _process_package_check_symlink(entry, filename):

        if not filename:
            match = re.match(r'.*symlinked ([^\s]+) to it', entry['messages'])
            if match:
                filename = match.group(1)
        return filename

    @staticmethod
    def _process_package_check_downloaded(entry, filename):

        if not filename:
            match = re.match(r'Successfully downloaded updated package '
                             r'(.+)', entry['messages'])
            if match:
                filename = match.group(1)
        return filename

    @staticmethod
    def _process_package_check_orig(package, filename):

        def _get_ext(package):

            for url in package.url:
                parts = os.path.splitext(url)
                if len(parts) > 1:
                    return parts[1]
            for tarball in package.tarball:
                parts = os.path.splitext(tarball)
                if len(parts) > 1:
                    return parts[1]
            return ''

        if not filename:
            ext = _get_ext(package)
            if package.package and package.version and ext:
                filename = '{0.package}_{0.version}.orig.tar{1}'.format(
                    package, ext)
        return filename

    @staticmethod
    def _process_package_set_tarball(package, destdir, filename):

        # There is only one component right now
        url, tarball = package.components.pop()
        if filename:
            fullpath = os.path.join(destdir, filename)
            if os.path.exists(fullpath):
                tarball = fullpath

        if (not tarball) and package.package and package.version:
            filename_glob = '{0.package}_{0.version}.orig.tar.*'.format(
                package)
            wild = os.path.join(destdir, filename_glob)
            files = glob.glob(wild)
            if files:
                filename = os.path.basename(files[0])
                tarball = files[0]

        if (not tarball) and url:
            filename = url.rsplit('/', 1)[1]
            fullpath = os.path.join(destdir, filename)
            if os.path.exists(fullpath):
                tarball = fullpath

        package.add(url, tarball)

        return filename

    def _process_package(self, entry, destdir):

        package = self.Package(entry)

        package.add(entry['upstream-url'], entry['target-path'])

        filename = entry['target']
        filename = self._process_package_check_repacked(entry, package, filename)
        filename = self._process_package_check_symlink(entry, filename)
        filename = self._process_package_check_downloaded(entry, filename)
        filename = self._process_package_check_downloaded(entry, filename)
        filename = self._process_package_check_orig(package, filename)
        filename = self._process_package_set_tarball(package, destdir, filename)

        package.filename = filename
        return package

    def _parse(self, out, destdir=None):
        r'''
        Parse the uscan output return and update the object's properties

        @param out: uscan output
        @type out: string

        >>> u = Uscan('http://example.com/')
        >>> u._parse('<target>virt-viewer_0.4.0.orig.tar.gz</target>')
        >>> u.tarball.pop()
        '../virt-viewer_0.4.0.orig.tar.gz'
        >>> u.uptodate
        False
        >>> u._parse('')
        Traceback (most recent call last):
        ...
        UscanError: Couldn't find 'upstream-url' in uscan output
        >>> u._parse('<dehs><warnings>uscan: no watch file found</warnings></dehs>')
        Traceback (most recent call last):
        ...
        UscanError: Uscan warning: uscan: no watch file found
        '''
        xml_root = lxml.etree.fromstring(out)
        if xml_root.tag != 'dehs':
            raise UscanError(
                'Unexpected uscan output, missing dehs tag: {}'.format(out))
        packages = []
        current = None
        for child in xml_root:
            # logging.info('_parse: %s', child.tag)
            if child.tag == 'package':
                current = defaultdict(str)
                packages.append(current)
            if current is None:
                if child.tag == 'warnings':
                    raise UscanError('Uscan warning: {}'.format(child.text))
                raise UscanError('Unexpected uscan output: {}'.format(out))
            current[child.tag] = child.text
        # logging.info('%s', str(packages))

        if not destdir:
            destdir = self._destdir

        latest = self._parse_get_latest(packages, destdir)
        if not latest:
            raise UscanError('Unexpected uscan output: {}'.format(out))

        self._result = latest
        self.uptodate = (latest.status == 'up to date')

    def _parse_get_latest(self, packages, destdir):

        latest = None
        for entry in packages:
            package = self._process_package(entry, destdir)

            if not package.version:
                continue
            if not latest:
                latest = package
                continue

            cmp_res = ds.version_compare(latest.version, package.version)
            if cmp_res < 0:
                latest = package
            elif cmp_res == 0:
                latest.update(package)

        return latest

    @staticmethod
    def _raise_error(out):
        r'''
        Parse the uscan output for errors and warnings and raise
        a L{UscanError} exception based on this. If no error detail
        is found a generic error message is used.

        @param out: uscan output
        @type out: string
        @raises UscanError: exception raised

        >>> u = Uscan('http://example.com/')
        >>> u._raise_error("<warnings>uscan warning: "
        ... "In watchfile debian/watch, reading webpage\n"
        ... "http://a.b/ failed: 500 Cant connect "
        ... "to example.com:80 (Bad hostname)</warnings>")
        Traceback (most recent call last):
        ...
        UscanError: Uscan failed: uscan warning: In watchfile debian/watch, reading webpage
        http://a.b/ failed: 500 Cant connect to example.com:80 (Bad hostname)
        >>> u._raise_error("<errors>uscan: Can't use --verbose if "
        ... "you're using --dehs!</errors>")
        Traceback (most recent call last):
        ...
        UscanError: Uscan failed: uscan: Can't use --verbose if you're using --dehs!
        >>> u = u._raise_error('')
        Traceback (most recent call last):
        ...
        UscanError: Uscan failed - debug by running 'uscan --verbose'
        '''
        msg = None

        for tag_name in ('errors', 'warnings'):
            match = re.search('<{0}>(.*)</{0}>'.format(tag_name), out, re.DOTALL)
            if match:
                msg = 'Uscan failed: {}'.format(match.group(1))
                break

        if not msg:
            msg = "Uscan failed - debug by running 'uscan --verbose'"
        raise UscanError(msg)

    def scan(self, destdir=None, download=True, force_download=False,
             version=None):
        '''Invoke uscan to fetch a new upstream version'''
        if not destdir:
            destdir = self._destdir
        util.ensure_path(destdir)
        cmd = [self.cmd, '--symlink', '--destdir={}'.format(destdir),
               '--dehs', '--watchfile', 'debian/watch']
        if not download:
            cmd.append('--report')
        if download and force_download or download and version:
            cmd.append('--force-download')
        if version:
            cmd.append('--download-version={}'.format(version))

        logging.debug('Calling uscan: %s', cmd)
        proc = subprocess.Popen(cmd, cwd=self._dir, universal_newlines=True,
                                stdout=subprocess.PIPE)
        out = proc.communicate()[0]
        logging.debug('uscan output:\n%s', out)
        # uscan exits with 1 in case of uptodate and when an error occured.
        # Don't fail in the uptodate case:
        self._parse(out, destdir)
        if not self.uptodate and proc.returncode:
            self._raise_error(out)
        if download and not any(self.tarball):
            raise UscanError("Couldn't find tarball")

# vi:expandtab:softtabstop=4:shiftwidth=4:smarttab
