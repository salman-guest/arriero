#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright: 2015, Maximiliano Curia <maxy@debian.org>
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
'Arriero exception classes'
##
# Exceptions
##


class ArrieroError(Exception):
    'Arriero exception base class'
    pass


class ActionError(ArrieroError):
    'Arriero action error'
    pass


class GitError(ArrieroError):
    'Arriero git error'
    pass


class GitBranchError(GitError):
    'Arriero git branch error'
    pass


class GitDirty(GitError):
    'Arriero git worktree error'
    pass


class GitDiverge(GitError):
    'Arriero git diverge error'
    pass


class GitRemoteNotFound(GitError):
    'Arriero git remote error'
    pass


class PackageError(ArrieroError):
    'Arriero package error'
    pass


class TestFailed(ArrieroError):
    'Arriero test error'
    pass


class UscanError(ArrieroError):
    'Arriero uscan error'
    pass
