#!/usr/bin/env python3
# -*- coding: utf8 -*-
# Copyright: 2016, Maximiliano Curia <maxy@debian.org>
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

import re

import debian.debian_support as ds


class Version(ds.Version):

    '''Represents the different parts of the version string.

    Attributes:
        full_version    : Full version string
        epoch           : Epoch if any, None otherwise
        upstream_version: Upstream version
        debian_revision : Debian revision if any, None otherwise
    '''
    known_vendors = {'neon', 'tanglu', 'ubuntu'}
    vendor_re = re.compile(r'\d(?P<vendor>[a-zA-Z]+)?(?P<revision>[0-9.~]*)$')

    def __init__(self, version_string):
        super(ds.Version, self).__init__(version_string)

    @staticmethod
    def from_parts(epoch=None, upstream_version='0', debian_revision=None):
        version_string = ''
        if epoch:
            version_string += '%s:' % epoch
        version_string += upstream_version
        if debian_revision:
            version_string += '-%s' % debian_revision
        return Version(version_string)

    @staticmethod
    def from_tag(tag):
        return Version(tag.replace('_', '~').replace('%', ':').replace('#', ''))

    @staticmethod
    def to_tag(version):
        tag = str(version).replace('~', '_').replace(':', '%')
        return re.sub(r'\.(?=\.|$|lock$)', '.#', tag)

    def is_native(self):
        return not self.debian_revision

    @property
    def vendor(self):
        if self.is_native():
            version = self.upstream_version
        else:
            version = self.debian_revision
        match = self.vendor_re.search(version)
        if match and match.group('vendor') in self.known_vendors:
            return match.group('vendor')
        return ''

    @property
    def vendor_revision(self):
        if self.is_native():
            version = self.upstream_version
        else:
            version = self.debian_revision
        match = self.vendor_re.search(version)
        if match and match.group('vendor') in self.known_vendors:
            return match.group('revision')
        return ''

    def new_upstream_version(self, upstream_version):
        '''Returns a Version for the new upstream release.'''
        # Can be ''
        epoch = '%s:' % self.epoch if self.epoch else ''
        # Let's trust upstream
        # if dfsg_re.match(self.upstream) and dfsg_re.match(upstream):
        #    upstream += '+dfsg'
        debian_revision = (
            '-{}1~'.format(
                '0{}'.format(self.vendor) if self.vendor else ''
            ) if self.debian_revision else ''
        )

        new_version = epoch + upstream_version + debian_revision
        return Version(new_version)

    @staticmethod
    def _bump(revision):
        '''Bump a revision so dpkg --compare-version considers it greater.'''
        match = re.match('(.*?)([0-9]*)$', revision)

        numeric_part = 0
        if match.group(2):
            numeric_part = int(match.group(2))
        numeric_part += 1

        return match.group(1) + str(numeric_part)

    def is_backport(self):
        '''Check if its a backport release.'''
        return (self.debian_revision and 'bpo' in self.debian_revision) or \
            (not self.debian_revision and 'bpo' in self.upstream_version)

    def is_pre_release(self):
        '''Check if its a pre release.'''
        return (self.debian_revision and '~' in self.debian_revision) or \
            (not self.debian_revision and '~' in self.upstream_version)

    def pre_release(self):
        '''Returns a pre release Version from the current one.'''

        def bump_pre(revision):
            if '~' not in revision:
                return revision + '~'
            if 'bpo' in revision:
                base, rest = revision.split('bpo', 1)
                return base + 'bpo' + bump_pre(rest)
            base, pre = revision.split('~', 1)
            return base + '~' + Version._bump(pre)

        if self.is_native():
            new_upstream_version = bump_pre(self.upstream_version)
            new_debian_revision = self.debian_revision
        else:
            new_upstream_version = self.upstream_version
            new_debian_revision = bump_pre(self.debian_revision)
        return Version.from_parts(self.epoch, new_upstream_version,
                                  new_debian_revision)

    def release(self):
        '''Returns a release Version from a pre release one.'''

        def remove_pre(revision):
            pos = revision.find('~')
            if pos == -1:
                return revision
            return revision[:pos]

        if self.is_native():
            new_upstream_version = remove_pre(self.upstream_version)
            new_debian_revision = self.debian_revision
        else:
            new_upstream_version = self.upstream_version
            new_debian_revision = remove_pre(self.debian_revision)

        return Version.from_parts(self.epoch, new_upstream_version,
                                  new_debian_revision)
