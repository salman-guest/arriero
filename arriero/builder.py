#!/usr/bin/env python3
# -*- coding: utf8 -*-
# Copyright: 2013-2014, Maximiliano Curia <maxy@debian.org>
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

from __future__ import print_function
import getpass
import logging
import os
import re
import random
import shutil
import string
import subprocess
import sys
import tempfile
import time

from pkg_resources import Requirement, resource_filename

from . import util
from .constants import (NO, ON_ERROR, ALWAYS)
from .errors import GitError, TestFailed


class Common(object):

    def __init__(self, arriero, dist, arch):
        self._arriero = arriero
        self.dist = dist.lower()
        self.arch = arch

        # Update the chroot just once (if we are planning to make this a long
        # running process, then it might make sense to set a ttl.
        self._updated = False

    def create(self):
        pass

    def update(self, force=False):
        self._updated = True

    @property
    def config(self):
        return self._arriero.config


class Builder(Common):

    def build(self, package):
        pass

    def ensure_image(self):
        try:
            self.update()
        except Exception:
            # FIXME: this gets called even if a package fails to be installed
            # we need to avoid calling create if the builder is already
            # created
            # self.create()
            pass

    def get_hooks_dir(self):
        # TODO: use the installed path if installed, is there a way to set a
        # variable after installing?
        dirname = resource_filename(Requirement.parse("Arriero"), "hooks")
        return dirname

    def _get_shell_enum(self, hooks):

        shell = NO
        if 'shell' in hooks:
            shell = ALWAYS
        elif 'shell_on_error' in hooks:
            shell = ON_ERROR
        return shell

    @staticmethod
    def _get_gbp_component_args(package):
        args = []
        if package.pristine_tar:
            cmd = ['pristine-tar', 'list']
            result = util.log_run(cmd, cwd=package.path)
            if result.returncode:
                return args
            for line in util.iter_lines(result.stdout):
                line = line.rstrip('\n')
                m = re.search(r'{}_{}.orig-(.*?)\.'.format(
                    package.source_name, package.upstream_version), line)
                if m:
                    component = m.group(1)
                    args.append('--git-component={}'.format(component))

        return args

    @staticmethod
    def tag(package, ignore_branch):

        tag_name = package.tag_template('debian')
        try:
            existing_tag = package.repo.tag('refs/tags/{}'.format(tag_name))
        except Exception:
            existing_tag = None

        if existing_tag in package.repo.tags:
            if existing_tag.commit == package.repo.head.commit:
                logging.info('%s: tag %s already present pointing to HEAD',
                             package.name, tag_name)
                return
            else:
                raise GitError(
                    '{}: tag {} already present'.format(
                        package.name, tag_name
                    )
                )

        cmd = [
            'gbp', 'buildpackage', '--git-tag-only', '--git-sign-tags',
        ]
        if package.get('verbose'):
            cmd.append('--git-verbose')
        if not ignore_branch:
            cmd.append('--git-debian-branch={}'.format(package.debian_branch))
        cmd.append(
            '--git-{}ignore-new'.format(
                '' if package.get('ignore_new') else 'no-'
            )
        )
        cmd.append(
            '--git-debian-tag={}'.format(
                tag_name.replace('%', '%%')
            )
        )
        tag_msg = package.get('debian_tag_msg')
        if tag_msg:
            cmd.append('--git-debian-tag-msg={}'.format(tag_msg))
        util.log_check_call(cmd, cwd=package.path, interactive=True)

    def _prepare_hooks(self, package):
        hooks = package.get('builder_hooks')
        user_hooks_dir = package.get('builder_user_hooks_dir')

        shell = self._get_shell_enum(hooks)

        if not user_hooks_dir and not hooks:
            return None, shell

        tmpdir = tempfile.mkdtemp(prefix='builder', suffix='hooks')
        os.chmod(tmpdir, 0o755)
        if user_hooks_dir:
            cmd = ['cp', '-Lrx', os.path.join(user_hooks_dir, '.'), tmpdir]
            util.quiet(cmd)
        if hooks:
            hooks_dir = self.get_hooks_dir()
            for hook in hooks:
                src = os.path.join(hooks_dir, hook)
                if os.path.isdir(src):
                    cmd = ['cp', '-Lrx', os.path.join(src, '.'), tmpdir]
                    util.quiet(cmd)
                else:
                    # TODO: ERROR ?
                    # ignoring invalid hook
                    logging.warning('%s: Ignoring invalid hook: %s',
                                    package.name, hook)
                    pass
        return tmpdir, shell

    def source_build(self, package, ignore_branch=False):

        cmd = [
            'gbp', 'buildpackage',
            '--git-export-dir={}'.format(package.export_dir),
            '-S',
            '--git-builder=debuild -I -i -us -uc -d '
            '--changes-option=-DDistribution={}'.format(
                package.target_distribution),
        ]
        force_orig_source = package.get('force_orig_source')
        if force_orig_source:
            cmd.append('-sa')

        if package.get('verbose'):
            cmd.append('--git-verbose')
        if package.pristine_tar:
            cmd.append('--git-pristine-tar')
        # If already generated, use that
        cmd.append('--git-tarball-dir={}'.format(package.tarball_dir))
        cmd.append('--git-upstream-tag={}'.format(
            package.tag_template('upstream', upstream_version='%(version)s')))

        if not ignore_branch:
            cmd.append('--git-debian-branch={}'.format(package.debian_branch))
        cmd.append('--git-{}ignore-new'.format(
            '' if package.get('ignore_new') else 'no-'))

        # if not package.is_merged and not package.is_native():
        #    cmd.append('--git-overlay')

        cmd += self._get_gbp_component_args(package)

        env = {}

        tmp_hooks_dir, shell = self._prepare_hooks(package)
        if tmp_hooks_dir:
            gbp_options = []
            if 'GIT_PBUILDER_OPTIONS' in os.environ:
                gbp_options.append(os.environ['GIT_PBUILDER_OPTIONS'])
            gbp_options.append('--hookdir={}'.format(tmp_hooks_dir))
            env['GIT_PBUILDER_OPTIONS'] = ' '.join(gbp_options)

        builder_options = package.get('builder_options')
        if builder_options:
            cmd.extend(builder_options)

        try:
            util.log_check_call(cmd, cwd=package.path,
                                env=dict(os.environ, **env), interactive=True)
        finally:
            if tmp_hooks_dir and os.path.exists(tmp_hooks_dir):
                shutil.rmtree(tmp_hooks_dir)


class CowBuilder(Common):
    updated = set()

    def __init__(self, arriero, dist, arch, *a, **kw):
        super().__init__(arriero, dist, arch, *a, **kw)

        self.basepath = self.config.get('cowbuilder_basepath',
                                        distribution=dist,
                                        architecture=arch)

    def create(self):
        self._updated = True

        env = {'ARCH': self.arch, 'DIST': self.dist}
        if self.dist == 'unreleased':
            options = []
            if 'GIT_PBUILDER_OPTIONS' in os.environ:
                options.append(os.environ['GIT_PBUILDER_OPTIONS'])
            # TODO: Fix this
            options.append('--distribution=sid')
            ' '.join(options)
            env['GIT_PBUILDER_OPTIONS'] = ' '.join(options)

        logging.warning('Build image for %s-%s not found. Creating...',
                        self.dist, self.arch)
        cmd = ['git-pbuilder', 'create']
        util.log_check_call(cmd, interactive=True, env=dict(os.environ, **env))

    def needs_update(self):
        if not self.basepath:
            return
        return self.basepath not in self.updated

    def mark_updated(self):
        return self.updated.add(self.basepath)

    def update(self, force=False):
        if not force and self._updated:
            return
        self._updated = True
        if not force and not self.needs_update():
            return
        self.mark_updated()
        update = self.config.get('update_build_image')
        if not update:
            return
        env = {'ARCH': self.arch, 'DIST': self.dist}
        cmd = ['git-pbuilder', 'update']
        logging.info('Updating build image for %s-%s', self.dist, self.arch)
        util.log_check_call(cmd, interactive=True, env=dict(os.environ, **env))


class GitPBuilder(CowBuilder, Builder):

    def build(self, package, ignore_branch=False):

        source_only = package.get('source_only')
        if source_only:
            return self.source_build(package, ignore_branch)

        env = {}
        cmd = self._build_cmd(package, ignore_branch)

        tmp_hooks_dir, shell = self._prepare_hooks(package)
        if tmp_hooks_dir:
            gbp_options = []
            if 'GIT_PBUILDER_OPTIONS' in os.environ:
                gbp_options.append(os.environ['GIT_PBUILDER_OPTIONS'])
            gbp_options.append('--hookdir={}'.format(tmp_hooks_dir))
            env['GIT_PBUILDER_OPTIONS'] = ' '.join(gbp_options)

        builder_options = package.get('builder_options')
        if builder_options:
            cmd.extend(builder_options)

        try:
            util.log_check_call(cmd, cwd=package.path,
                                env=dict(os.environ, **env), interactive=True)
        finally:
            if tmp_hooks_dir and os.path.exists(tmp_hooks_dir):
                shutil.rmtree(tmp_hooks_dir)

        package.test(shell=shell, ignore_branch=ignore_branch)

    @staticmethod
    def _get_build_arg(source, arch_all, arch_any):
        # This is way simpler using the --build argument, but that's not
        # available in stretch

        if not source:
            if not arch_all and arch_any:
                return '-B'
            if arch_all and not arch_any:
                return '-A'
            if arch_all and arch_any:
                return '-b'
        else:
            if not arch_all and arch_any:
                return '-G'
            if arch_all and not arch_any:
                return '-g'
            if arch_all and arch_any:
                return '-F'
            if not arch_all and not arch_any:
                return '-S'
        # Nothing to do?

    def _build_cmd(self, package, ignore_branch):

        cmd = [
            'gbp', 'buildpackage',
            '--git-export-dir={}'.format(package.export_dir),
            '--git-arch={}'.format(self.arch),
            '--git-dist={}'.format(self.dist),
        ]
        force_orig_source = package.get('force_orig_source')
        if force_orig_source:
            cmd.append('-sa')

        cmd.append('--git-pbuilder')

        source = not package.get('binary_only')
        arch_all = package.get('arch_all')
        arch_any = package.get('arch_any')

        build_arg = self._get_build_arg(source, arch_all, arch_any)
        if not build_arg:
            # Nothing to do
            return ['true'], True
        cmd.append(build_arg)

        if package.get('verbose'):
            cmd.append('--git-verbose')
        if package.pristine_tar:
            cmd.append('--git-pristine-tar')
        # If already generated, use that
        cmd.append('--git-tarball-dir={}'.format(package.tarball_dir))

        if not ignore_branch:
            cmd.append('--git-debian-branch={}'.format(package.debian_branch))
        cmd.append('--git-{}ignore-new'.format(
            '' if package.get('ignore_new') else 'no-'))

        # if not package.is_merged and not package.is_native():
        #    cmd.append('--git-overlay')

        cmd += self._get_gbp_component_args(package)

        cmd += [
            '--changes-option=-DDistribution={}'.format(
                package.target_distribution),
        ]

        return cmd


class SChRoot(Common):
    updated = set()

    def __init__(self, arriero, dist, arch, *args, **kwargs):
        super().__init__(arriero, dist, arch, *args, **kwargs)
        self.schroot = self.config.get('sbuild_chroot', distribution=dist,
                                       architecture=arch)

    def create(self):
        self._updated = True

        chrootpath = self.config.get('sbuild_chrootpath')

        if self.dist == 'unreleased':
            distribution = 'sid'
        else:
            distribution = self.dist

        # TODO: remove hardcoded mirror
        cmd = ['sudo', 'sbuild-createchroot', '--arch={}'.format(self.arch),
               distribution, chrootpath,
               'http://deb.debian.org/debian']
        # TODO: fix image:
        #  - add local repositories
        #  - set fstab pkg_archive / apt-cacher-ng
        #  - configure ccache
        #  - configure tmpfs

        util.log_check_call(cmd, interactive=True)

    def needs_update(self):
        if not self.schroot:
            return
        return self.schroot not in self.updated

    def mark_updated(self):
        return self.updated.add(self.schroot)

    def update(self, force=False):
        if not force and self._updated:
            return
        self._updated = True
        if not force and not self.needs_update():
            return
        self.mark_updated()
        update = self.config.get('update_build_image')
        if not update:
            return
        cmd = ['sudo', 'sbuild-update', '-uagdr', self.schroot]
        util.log_check_call(cmd, interactive=True)

    def shell_in_session(self, session, start_dir='/'):
        logging.info('Starting shell in session: %s '
                     'directory: %s', session, start_dir)
        if not session:
            # No session, probably it could not even be started
            return
        cmd = ['schroot', '-c', session, '--shell=/bin/bash',
               '--directory={}'.format(start_dir), '-r']
        util.log_check_call(cmd, cwd='/', interactive=True)

    def end_session(self, session):
        if not session:
            # No session, probably it could not even be started
            return
        cmd = ['schroot', '-c', session, '-e']
        util.log_check_call(cmd, interactive=True)


class SBuild(SChRoot, Builder):

    class SBuildSession(object):

        def __init__(self, sbuild, package, cmd):
            self.sbuild = sbuild
            self.package = package
            self.cmd = cmd
            self._session_path = None
            self._build_dir = None
            self.exc_info = None

        def __enter__(self):
            try:
                util.log_check_call(self.cmd,
                                    cwd=self.package.export_dir,
                                    interactive=True)
            except subprocess.CalledProcessError:
                self.exc_info = sys.exc_info()
            return self

        def process_buildlog(self):
            build_dir_re = re.compile(
                r"I: NOTICE: Log filtering will replace '(.*)' with "
                r"'(?:«|<<)PKGBUILDDIR(?:»|>>)'")
            session_path_re = re.compile(
                r"I: NOTICE: Log filtering will replace '(.*)' with "
                r"'(?:«|<<)CHROOT(?:»|>>)'")
            try:
                buildlog = open(self.package.build_file)
            except IOError:
                # No build file, most probably the session could not be
                # started
                return
            for line in buildlog:
                match = session_path_re.match(line)
                if match:
                    self._session_path = match.group(1)
                match = build_dir_re.match(line)
                if match:
                    self._build_dir = match.group(1)
                if self._session_path and self._build_dir:
                    break

        @property
        def session_path(self):
            if not self._session_path:
                self.process_buildlog()
            if self._session_path and not self._session_path.startswith('/'):
                self._session_path = '/' + self._session_path
            return self._session_path

        @property
        def build_dir(self):
            if not self._build_dir:
                self.process_buildlog()
            return self._build_dir

        @property
        def chroot_build_dir(self):
            if self.full_build_dir and os.path.exists(self.full_build_dir):
                return os.path.join('/', self.build_dir)
            return '/'

        @property
        def session(self):
            path = self.session_path
            if path is None:
                return
            return os.path.basename(path)

        @property
        def full_build_dir(self):
            path = self.session_path
            if path is None:
                return
            build_dir = self.build_dir
            if not build_dir:
                return
            return os.path.join(self.session_path, self.build_dir)

        def __exit__(self, exc_type, exc_value, traceback):
            self.sbuild.end_session(self.session)

    def check_lintian(self, package):
        error_re = re.compile(r'Lintian: (fail|error)')
        try:
            buildlog = open(package.build_file)
            for line in buildlog:
                match = error_re.match(line)
                if match:
                    raise TestFailed("Lintian failed")
        except Exception:
            raise TestFailed("Couldn't open build log")

    def run_sbuild(self, package, sbuild_options,
                   ignore_branch, binary_only):

        sbuild_cmd = self._run_sbuild_sbuild_cmd(package, binary_only,
                                                 sbuild_options)

        return self.SBuildSession(self, package, sbuild_cmd)

    def build(self, package, ignore_branch=False):

        self.source_build(package, ignore_branch)

        if package.get('source_only'):
            return

        hooks = package.get('builder_hooks')
        shell = self._get_shell_enum(hooks)

        sbuild_options = package.get('sbuild_options')
        binary_only = package.get('binary_only')

        with self.run_sbuild(package, sbuild_options,
                             ignore_branch, binary_only) as session:
            exc_info = session.exc_info

            try:
                if exc_info:
                    raise exc_info[0].with_traceback(exc_info[1], exc_info[2])
                self._build_run_tests(package, session,
                                      ignore_branch=ignore_branch,
                                      shell=shell,
                                      run_lintian=package.get('run_lintian'),
                                      hooks=hooks)
            except (subprocess.CalledProcessError, TestFailed):
                if shell in (ON_ERROR, ALWAYS):
                    self.shell_in_session(session.session,
                                          start_dir=session.chroot_build_dir)
                raise

            if shell == ALWAYS:
                self.shell_in_session(session.session,
                                      start_dir=session.chroot_build_dir)

    def _run_sbuild_sbuild_cmd(self, package, binary_only,
                               sbuild_options):

        sbuild_cmd = [
            'sbuild',
            '--dist={}'.format(package.target_distribution),
            '--arch={}'.format(self.arch),
            '--chroot={}'.format(self.schroot),
        ]
        sbuild_cmd.append('--{}arch-all'.format(
            '' if package.get('arch_all') else 'no-'))
        sbuild_cmd.append('--{}arch-any'.format(
            '' if package.get('arch_any') else 'no-'))

        if not binary_only:
            sbuild_cmd.append('--source')

        if package.get('force_orig_source'):
            sbuild_cmd.append('--force-orig-source')

        if package.get('verbose'):
            sbuild_cmd.append('--verbose')

        for repository in package.get('extra_repository'):
            sbuild_cmd.append('--extra-repository={}'.format(repository))

        if package.get('run_lintian'):
            sbuild_cmd.extend([
                '--run-lintian',
                '--lintian-opts={}'.format(
                    util.join(package.get('lintian_options')))])
        else:
            sbuild_cmd.append('--no-run-lintian')

        if package.get('run_piuparts'):
            sbuild_cmd.append('--run-piuparts')
            piuparts_opts = '--schroot={}'.format(self.schroot)
            if self.dist != 'unreleased':
                piuparts_opts = '-d {} {}'.format(self.dist, piuparts_opts)
            # eatmydata can't be run with eatmydata, and full schroot setup
            # would run eatmydata, so dropping it here.
            piuparts_opts = "--no-eatmydata {}".format(piuparts_opts)
            sbuild_cmd.append('--piuparts-opts="{}"'.format(piuparts_opts))
        else:
            sbuild_cmd.append('--no-run-piuparts')

        sbuild_cmd.append('--purge=never')

        sbuild_cmd.extend(sbuild_options)

        sbuild_cmd.append(package.dsc_file)

        return sbuild_cmd

    def _build_run_tests(self, package, session,
                         ignore_branch, shell, run_lintian, hooks):

        if run_lintian:
            self.check_lintian(package)

        # lintian and piuparts are handled by sbuild
        package.test(ignore_branch=ignore_branch, shell=shell,
                     run_lintian=False, run_piuparts=False)

        hooks_dir = self.get_hooks_dir()
        errors = []
        for hook in hooks:
            if hook in {'shell', 'shell_on_error'}:
                # Handled in a different way for sbuild
                continue
            hook_dir = os.path.join(hooks_dir, hook)
            hook_script = os.path.join(hook_dir, hook)
            if (not os.path.isfile(hook_script) or
                    not os.access(hook_script, os.X_OK)):
                # TODO: ERROR ?
                # ignoring invalid hook
                logging.warning('%s: Ignoring invalid hook: %s',
                                package.name, hook)
                continue
            cmd = [hook_script]
            env = {}
            env['SOURCE_CHANGES_FILE'] = package.source_changes_file
            env['CHANGES_FILE'] = package.changes_file
            env['EXPORT_DIR'] = package.export_dir
            env['BUILDLOG'] = package.build_file
            returncode = util.log_check_call(cmd,
                                             cwd=session.full_build_dir,
                                             interactive=True,
                                             env=dict(os.environ, **env))
            if returncode:
                errors.append('hook "{}" failed with returncode {}'.format(
                    hook, returncode))
        if errors:
            raise TestFailed('\n'.join(errors))


class Tester(Common):

    def test(self, package, changes_file, shell=NO, run_blhc=None,
             run_lintian=None, run_piuparts=None, run_autopkgtest=None):

        # TODO: Avoid launching the shell twice after build if a test fails
        extra_repositories = package.get('extra_repository')

        if package.get('run_blhc', run_blhc=run_blhc):
            self.run_blhc(package, changes_file, extra_repositories)

        if package.get('run_lintian', run_lintian=run_lintian):
            self.run_lintian(package, changes_file, extra_repositories)

        if package.get('run_piuparts', run_piuparts=run_piuparts):
            self.run_piuparts(package, changes_file, extra_repositories,
                              shell=shell)

        run_autopkgtest = (
            package.get('run_autopkgtest',
                        run_autopkgtest=run_autopkgtest) and
            package.has_tests())
        if run_autopkgtest:
            self.run_autopkgtest(package, changes_file, extra_repositories,
                                 shell=shell)

    def run_autopkgtest(self, package, changes_file, extra_repositories,
                        shell=NO,
                        cmd=None, args=None, extra=None):

        # Create the script in the build_dir so that, if bindmounted it canbe
        # executed

        dirname = os.path.dirname(changes_file)
        script = self.extra_repository_script(extra_repositories,
                                              dirname=dirname)

        if cmd is None:
            cmd = ['autopkgtest']
        if args is None:
            args = ['-U']
            user_args = package.get('autopkgtest_args', user=getpass.getuser())
            if user_args:
                args += user_args
            if shell:
                args.append('--shell-fail')
            if shell == ALWAYS:
                args.append('--shell')
            # This could be useful to debug autopkgtest
            # if package.get('debug'):
            #     args.append('--debug')
            if script:
                args.append('--setup-commands={}'.format(script.name))
        cmd.extend(args)
        if extra is None:
            virt_args = []
            # This could be useful to debug autopkgtest, but way too verbose
            # if package.get('debug'):
            #     virt_args.append('--debug')
            extra = [changes_file, '--', 'null'] + virt_args
        cmd.extend(extra)

        try:
            util.log_check_call(cmd, interactive=True, cwd=package.path)
        finally:
            if script:
                script.close()

    def run_blhc(self, package, changes_file, extra_repositories,
                 cmd=None, args=None, extra=None):

        if cmd is None:
            cmd = ['blhc']
        if args is None:
            args = ['--all']
        cmd += args
        if extra is None:
            extra = [package.build_file]
        cmd += extra
        util.log_check_call(cmd, cwd=package.path)

    def run_lintian(self, package, changes_file, extra_repositories,
                    cmd=None, args=None, extra=None):

        if cmd is None:
            cmd = ['lintian']
        if args is None:
            args = package.get('lintian_options')
        cmd.extend(args)
        if extra is None:
            extra = [changes_file]
        cmd.extend(extra)
        util.log_check_call(cmd, cwd=package.path)

    def run_piuparts(self, package, changes_file, extra_repositories,
                     shell=NO,
                     cmd=None, args=None, extra=None):

        if cmd is None:
            cmd = ['sudo', 'piuparts']
        if args is None:
            args = ['--arch={}'.format(self.arch),
                    '--warn-on-debsums-errors']
            if self.dist != 'unreleased':
                args.append('--distribution={}'.format(self.dist))
        if shell:
            args.append('--shell-on-error')
        for extra_repo in extra_repositories:
            args.append("--extra-repo={}".format(extra_repo))
        cmd.extend(args)
        if extra is None:
            extra = [changes_file]
        cmd.extend(extra)

        util.log_check_call(cmd, cwd=package.path)

    def extra_repository_script(self, repositories, dirname):

        if not repositories:
            return

        script = tempfile.NamedTemporaryFile(prefix='repo-setup',
                                             suffix='.sh',
                                             dir=dirname)
        output = '/etc/apt/sources.list.d/extra_repository.list'
        lines = ['#!/bin/sh',
                 '',
                 "touch '{}'".format(output)]

        for repository in repositories:
            lines.append(
                "echo '{}' >> '{}'".format(repository, output)
            )
        lines.append('')
        lines.append('apt-get update')

        script.write('\n'.join(lines).encode('utf-8'))
        script.flush()

        return script


class CowBuilderTester(CowBuilder, Tester):

    def run_autopkgtest(self, package, changes_file, extra_repositories,
                        shell=NO,
                        cmd=None, args=None, extra=None):

        dirname = os.path.dirname(changes_file)

        with self.autopkgtest_script() as script:
            if cmd is None:
                cmd = ['sudo', 'cowbuilder', 'execute',
                       '--basepath', self.basepath,
                       '--bindmounts', dirname, '--',
                       script.name]
            super().run_autopkgtest(package, changes_file, extra_repositories,
                                    shell, cmd, args, extra)

    def autopkgtest_script(self):

        script = tempfile.NamedTemporaryFile(prefix='autopkgtest',
                                             suffix='.sh')
        lines = ['#!/bin/sh',
                 '',
                 'apt-get -y install autopkgtest',
                 'echo Running: autopkgtest "$@"',
                 'autopkgtest "$@"']

        script.write('\n'.join(lines).encode('utf-8'))
        script.flush()

        return script


class SChRootTester(SChRoot, Tester):

    def run_piuparts(self, package, changes_file, extra_repositories,
                     shell=NO,
                     cmd=None, args=None, extra=None):

        if extra is None:
            # eatmydata can't be run with eatmydata, and full schroot setup
            # would run eatmydata, so dropping it here.
            extra = ['--no-eatmydata',
                     "--schroot={}".format(self.schroot),
                     changes_file]

        super().run_piuparts(package, changes_file, extra_repositories, shell,
                             cmd, args, extra)

    def run_autopkgtest(self, package, changes_file, extra_repositories,
                        shell=NO,
                        cmd=None, args=None, extra=None):

        if extra is None:
            virt_args = []
            # This could be useful to debug autopkgtest, but way too verbose
            # if package.get('debug'):
            #     virt_args.append('--debug')
            virt_args.append(self.schroot)
            extra = [changes_file, '--', 'schroot'] + virt_args

        super().run_autopkgtest(package, changes_file, extra_repositories,
                                shell, cmd, args, extra)


class LXCTester(SChRootTester):

    def __init__(self, arriero, dist, arch, *a, **kw):
        super().__init__(arriero, dist, arch, *a, **kw)

        self.name = self.config.get('lxc_tester_name', distribution=dist,
                                    architecture=arch)

    def lxc_run(self, cmd, clone=True, shared_dir=None):
        container_name = self.name
        if clone:
            container_name = self._get_container_name(self.name)
        try:
            if clone:
                _cmd = ['sudo', 'lxc-copy', '--name', self.name,
                        '--newname', container_name]
                util.log_check_call(_cmd, interactive=True)
            _cmd = ['sudo', 'lxc-start', '-n', container_name, '--daemon']
            if shared_dir:
                _cmd += [
                    '--define',
                    'lxc.mount.entry={} {} none bind,create=dir 0 0'.format(
                        shared_dir, shared_dir[1:]),
                ]
            try:
                util.log_check_call(_cmd, interactive=True)
                self._wait_booted(container_name)
                _cmd = ['sudo', 'lxc-attach', '--name', container_name, '--']
                _cmd += cmd
                return util.log_check_call(_cmd, interactive=True)
            finally:
                _cmd = ['sudo', 'lxc-stop', '--quiet', '--kill',
                        '--name', container_name]
                util.log_check_call(_cmd, interactive=True)
        finally:
            if clone:
                _cmd = ['sudo', 'lxc-destroy', '--quiet',
                        '--name', container_name]
                util.log_check_call(_cmd, interactive=True)

    def update(self, force=False):
        if not force and self._updated:
            return
        self._updated = True
        update = self.config.get('update_build_image')
        if not update:
            return
        cmd = [
            'bash', '-c',
            '[ -r /etc/environment ] && . /etc/environment 2>/dev/null '
            '    || true; '
            '[ -r /etc/default/locale ] && . /etc/default/locale 2>/dev/null '
            '    || true; '
            '[ -r /etc/profile ] && . /etc/profile 2>/dev/null || true; '
            'set -e; '
            'export DEBIAN_FRONTEND=noninteractive; '
            'export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin; '
            'policy="/usr/sbin/policy-rc.d"; '
            'cleanup () { rm "$policy"; }; '
            'if ! [ -x "$policy" ]; then '
            '    trap cleanup EXIT; '
            '    echo exit 101 > "$policy"; '
            '    chmod +x "$policy"; '
            'fi; '
            'apt-get update; '
            'apt-get -y autoclean; '
            'apt-get -uy -o Dpkg::Options::=--force-confold '
            '    -o DPkg::Options::=--refuse-remove-essential '
            '    -o APT::Install-Recommends=false upgrade; '
            'apt-get -uy -o Dpkg::Options::=--force-confold '
            '    -o DPkg::Options::=--refuse-remove-essential '
            '    -o APT::Install-Recommends=false dist-upgrade; '
            'apt-get -y -o DPkg::Options::=--refuse-remove-essential '
            '    autoremove;'
        ]
        return self.lxc_run(cmd, clone=False)

    def run_lintian(self, package, changes_file, extra_repositories,
                    shell=NO,
                    cmd=None, args=None, extra=None):

        dirname = os.path.dirname(changes_file)
        script = self.extra_repository_script(extra_repositories,
                                              dirname=dirname)

        cmd = [
            'bash', '-c',
            '[ -r /etc/environment ] && . /etc/environment 2>/dev/null '
            '    || true; '
            '[ -r /etc/default/locale ] && . /etc/default/locale 2>/dev/null '
            '    || true; '
            '[ -r /etc/profile ] && . /etc/profile 2>/dev/null || true; '
            'set -e; '
            'apt-get update; '
            'apt-get -y install lintian; '
            'lintian {} {}; '.format(
                util.join(package.get('lintian_options')),
                changes_file),
        ]
        try:
            self.lxc_run(cmd, shared_dir=dirname)
        finally:
            if script:
                script.close()

    def run_autopkgtest(self, package, changes_file, extra_repositories,
                        shell=NO,
                        cmd=None, args=None, extra=None):

        if extra is None:
            virt_args = ['-s']
            # This could be useful to debug autopkgtest, but way too verbose
            # if package.get('debug'):
            #     virt_args.append('--debug')
            virt_args.append(self.name)
            extra = [changes_file, '--', 'lxc'] + virt_args

        super().run_autopkgtest(package, changes_file,
                                extra_repositories, shell,
                                cmd, args, extra)

    @staticmethod
    def _get_container_name(name):
        # If we get colliding names we should start buying lottery tickets
        return '{}-{}'.format(
            name,
            ''.join(
                random.choice(string.ascii_letters) for _ in range(16)))

    @staticmethod
    def _wait_booted(container_name):
        # Based on wait_booted from autopkgtest-virt-lxc
        timeout = 60
        while timeout > 0:
            timeout -= 1
            time.sleep(1)
            cmd = ['sudo', 'lxc-attach', '--name', container_name, 'runlevel']
            try:
                result = util.log_run(cmd, timeout=10, check=True)
            except Exception:
                continue
            runlevel = result.stdout.strip().split()[-1]
            if runlevel.isdigit():
                return runlevel
        raise Exception('wait_booted: timeout')


AVAILABLE_BUILDERS = {
    'git-pbuilder': (GitPBuilder, 'Use git-pbuilder cowbuilder'),
    'sbuild':       (SBuild,      'Use sbuild'),
}

AVAILABLE_TESTERS = {
    'cowbuilder':   (CowBuilderTester, 'Use cowbuilder'),
    'lxc':          (LXCTester,        'Use lxc'),
    'schroot':      (SChRootTester,    'Use schroot'),
}
