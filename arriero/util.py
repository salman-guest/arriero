#!/usr/bin/env python3
# -*- coding: utf8 -*-
# Copyright: 2013-2015, Maximiliano Curia <maxy@debian.org>
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

import collections
import fcntl
import io
import itertools
import logging
import os
import re
import signal
import string
import struct
import subprocess
import sys
import termios

import pexpect
import debian.deb822 as deb822

from .version import Version


# subprocess/pexpect wrappers

def pexpect_interact(cmd, **kwargs):
    '''Run command in a pty'''

    def sigwinch_passthrough(sig, data):
        s = struct.pack('HHHH', 0, 0, 0, 0)
        try:
            a = struct.unpack('hhhh', fcntl.ioctl(sys.stdout.fileno(),
                                                  termios.TIOCGWINSZ, s))
            p.setwinsize(a[0], a[1])
        except io.UnsupportedOperation:
            pass
        except OSError:
            # stdout might not be a tty
            pass

    if isinstance(cmd, str):
        cmd = [cmd]

    if 'shell' in kwargs:
        v = kwargs['shell']
        del kwargs['shell']
        if v:
            cmd = ['/bin/sh', '-c'] + cmd
    # Note this 'p' used in sigwinch_passthrough.
    p = pexpect.spawn(cmd[0], cmd[1:], **kwargs)
    old_handler = signal.signal(signal.SIGWINCH, sigwinch_passthrough)
    try:
        sigwinch_passthrough(signal.SIGWINCH, None)
        p.interact(None)
    finally:
        signal.signal(signal.SIGWINCH, old_handler)
        p.close()
    return p.exitstatus


def quiet(cmd, *argv, **kwargs):
    '''Make an OS call without generating any output.'''
    kw = chain_map(kwargs, universal_newlines=True, stdin=subprocess.DEVNULL,
                   stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
    return subprocess.call(cmd, *argv, **kw)


def log_popen(cmd, **kwargs):
    '''Equivalent to Popen, but with logging.'''
    str_cmd = cmd if isinstance(cmd, str) else ' '.join(cmd)
    kw = chain_map(kwargs, universal_newlines=True)
    logging.debug('Executing: %s with %s', str_cmd, kw)
    popen = subprocess.Popen(cmd, **kw)
    return popen


def log_run(cmd, timeout=None, **kwargs):
    '''Equivalent to subprocess.run, but logging the call'''
    str_cmd = cmd if isinstance(cmd, str) else ' '.join(cmd)
    # By default we capture the output, keep the error in the tty, and
    # redirect stdin to devnull for the subprocess, which should be sane
    # enough for scripted calls
    kw = chain_map(kwargs, universal_newlines=True, stdin=subprocess.DEVNULL,
                   stdout=subprocess.PIPE, stderr=None)
    logging.debug('Executing: %s with %s', str_cmd, kw)
    try:
        result = subprocess.run(cmd, **kw)
        logging.debug('\t%s exited with %d', str_cmd, result.returncode)
        logging.debug('stdout: %s\nstderr: %s', result.stdout, result.stderr)
    except subprocess.TimeoutExpired as e:
        logging.debug('\t%s timeout after %d seconds', str_cmd, e.timeout)
        logging.debug('stdout: %s\nstderr: %s', e.stdout, e.stderr)
        raise
    except subprocess.CalledProcessError as e:
        logging.debug('\t%s exited with %d', str_cmd, e.returncode)
        logging.debug('stdout: %s\nstderr: %s', e.stdout, e.stderr)
        raise
    except subprocess.SubprocessError as e:
        logging.debug('\t%s failed (%s)', str_cmd, e)
        raise
    return result


def log_call(cmd, interactive=False, check=True, **kwargs):
    '''Equivalent to call, but logging before and after.'''

    str_cmd = cmd if isinstance(cmd, str) else ' '.join(cmd)
    kw = chain_map(kwargs)
    if not interactive:
        kw['universal_newlines'] = True
    logging.debug('Executing: %s with %s', str_cmd, kw)
    if interactive:
        returncode = pexpect_interact(cmd, **kw)
    else:
        p = subprocess.run(cmd, **kw)
        returncode = p.returncode
    logging.debug('\t%s ended with returncode %s', str_cmd, returncode)
    if returncode and check:
        raise subprocess.CalledProcessError(returncode, cmd)
    return returncode


def log_check_call(cmd, interactive=False, **kwargs):
    return log_call(cmd, interactive=interactive, **kwargs)


# Debian specific

def version_at_distribution(source_name, distribution):
    result = rmadison(source_name, distribution=distribution)
    if not result:
        return None
    version = max(v for k, d in result.items() for v in d)
    return version


def rmadison(source_name, url='debian', distribution=None):
    cmd = ['rmadison', '--url={}'.format(url), source_name]
    if distribution:
        cmd.extend(['-s', distribution])
    logging.info('%s: %s', source_name, cmd)
    output = subprocess.check_output(cmd, universal_newlines=True)
    logging.info('%s: %s', source_name, output)
    result = {}
    for line in iter_lines(output):
        line = line.rstrip('\n')
        if '|' not in line:
            continue
        fields = line.split('|')
        version = fields[1].strip()
        # keep in mind, this is uses oldstable, stable, testing, unstable.
        # FIXME: We would need a way to learn these mappings.
        dist = fields[2].strip()
        result.setdefault(dist, []).append(Version(version))
    return result


# Simple helpers

class AttrDict(dict):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__dict__ = self


class ChainMap(collections.ChainMap):
    default_value = None

    def __getitem__(self, key):
        for mapping in self.maps:
            try:
                value = mapping[key]
                if value is not None:
                    return value
            except KeyError:
                pass
        return self.default_value


# https://stackoverflow.com/questions/19503455/caching-a-generator/19504173
class CachingIterable(object):

    def __init__(self, iterable):
        self.iterable = iterable
        self.iter = iter(iterable)
        self.done = False
        self.vals = []

    def __iter__(self):
        if self.done:
            return iter(self.vals)
        # chain vals so far & then gen the rest
        return itertools.chain(self.vals, self._gen_iter())

    def _gen_iter(self):
        # gen new vals, appending as it goes
        for new_val in self.iter:
            self.vals.append(new_val)
            yield new_val
        self.done = True


def chain_map(*ds, **kw):
    return ChainMap(*itertools.chain(ds, [kw]))


class OrderedSet(deb822.OrderedSet, collections.MutableSet):

    discard = deb822.OrderedSet.remove

    def __reversed__(self):
        # Return an iterator of items in the order they were added
        return reversed(self.__order)

    def pop(self, last=True):
        if not self:
            raise KeyError('pop from an empty set')
        key = self.__order.pop() if last else self.__order.pop(0)
        self.__set.remove(key)
        return key

    def __repr__(self):
        return '{}({})'.format(self.__class__.__name__, ', '.join(self))

    def __eq__(self, other):
        if isinstance(other, deb822.OrderedSet):
            return len(self) == len(other) and list(self) == list(other)
        return set(self) == set(other)


def iter_lines(s):
    start = 0
    end = 0
    while True:
        start, end = end, s.find('\n', end) + 1
        if end <= 0:
            break
        yield s[start:end]
    if s[start:]:
        yield s[start:]


# Filesystem

def ensure_path(path):
    '''Create path if it doesn't exist.'''
    if not os.path.exists(path):
        logging.info('Creating path: %s', path)
        os.makedirs(path)


# Configuration related

def str2bool(s):
    return s and s.lower() in ('true', 'on', 'yes')


def split(values):
    '''Split a comma separated string of values into a list.'''
    if values is None:
        values = ''
    return list(filter(lambda x: x is not '', re.split(r'[\s,]+', values)))


def join(values):
    '''Join values, this is intended to be used as the reverse of split.'''
    if values is None:
        return None
    return ' '.join(values)


def split_basename_suite(name):
    '''Split a package name of the form bash/stretch into bash, stretch'''
    if name is None:
        return None, None
    p = name.find('/')
    if p < 0:
        return name, None
    return name[:p], name[p + 1:]


def join_basename_suite(basename, suite):
    '''Join a package name from its basename and suite parts'''
    return '{}/{}'.format(basename, suite)


# String mangling

def expansions_needed(format_string):
    '''Get the amount and name of fields requested by a format string.

    Given a formated string in the "{}" syntax, returns the amount of
    positional arguments and named values it needs.
    '''
    formatter = string.Formatter()
    pos = 0  # positional arguments required
    fields = collections.OrderedDict()
    for (_, field_name, _, _) in formatter.parse(format_string):
        if field_name is None:
            continue  # just text
        if not field_name:
            pos += 1   # pos var
            continue
        fields[field_name] = None
    return pos, fields.keys()


# bfs

def bfs_gen(start, get_neighbors=None, visit=None):
    '''BFS generator implementation

    The starting position is not visited, to track the paths use a path as a
    starting point.

    :start: starting node
    :get_neighbors: function to obtain a sequence of reachable nodes
    :visit: function to be called on every visited node

    :returns: the node on the goal state or None.
    '''
    done = set([start])
    queue = collections.deque()
    if get_neighbors:
        queue.extend(get_neighbors(start))

    while queue:
        node = queue.popleft()
        if node in done:
            continue
        yield visit(node)

        done.add(node)

        queue.extend(get_neighbors(node))


# vi:expandtab:softtabstop=4:shiftwidth=4:smarttab
