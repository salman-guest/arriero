#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright: 2013-2017, Maximiliano Curia <maxy@debian.org>
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

from __future__ import print_function
import argparse
import configparser
import itertools
import logging
import os
import shutil

from collections import OrderedDict, defaultdict, namedtuple

from . import util
from .graph import TSortGraph

# types: string (default),
#        multistring (split('\n'), each term is expanded),
#        rawstring (do not expand format strings),
#        multiraw (split('\n'), each term is not expanded),
#        multivalue (util.split, not expanded),
#        int, bool (util.str2bool),
#        path (string with os.path.expanduser and normpath)
#        multipath (multistring with os.path.expanduser and normpath)
Schema = namedtuple('Schema', ('type', 'inherit'))
Schema.__new__.__defaults__ = ('string', True)


class Configuration(object):

    'Unified interface to configparser and argparse'

    _schema_types = set(('string', 'multistring', 'rawstring', 'multiraw',
                         'multivalue', 'int', 'bool', 'path', 'multipath'))
    _schema_raw_types = set(('rawstring', 'multiraw', 'multivalue'))
    _schema_list_types = set(('multistring', 'multivalue', 'multiraw', 'multipath'))

    def __init__(self,
                 defaults=None,
                 schema=None,
                 aliases=None,
                 argparse_kwargs=None,
                 configparser_kwargs=None):

        self.defaults = defaults if defaults else {}
        self.defaults.setdefault('config_files', [])
        self.schema = schema if schema else {}
        self.aliases = aliases if aliases else {}

        argparse_kwargs = argparse_kwargs if argparse_kwargs else {}
        configparser_kwargs = (
            configparser_kwargs if configparser_kwargs else {})

        self.argparser = argparse.ArgumentParser(
            **util.chain_map(argparse_kwargs,
                             fromfile_prefix_chars='@',
                             add_help=False))
        self.argparser.add_argument('-c', '--config',
                                    help='Specify a config file.',
                                    metavar='FILE', action='append',
                                    dest='config_files',
                                    default=list(self.defaults['config_files']))
        self.argparser.add_argument('--with',
                                    help='Override a configuration option.',
                                    nargs=3,
                                    metavar=('SECTION', 'KEY', 'VALUE'),
                                    action='append')
        self.config_files = None  # delay till we have to read the files

        # Let's handle the default section ourselves
        kwargs = util.chain_map(configparser_kwargs, default_section='')
        self.cfgparser = configparser.ConfigParser(**kwargs)
        self._args = None
        self._overrides = None

        self._members = None
        self._groups = None
        self._parents = None
        self._parents_cache = OrderedDict()

    @property
    def args(self):
        'Command line options'
        if not self._args:
            self._args = self.argparser.parse_args()
        return self._args

    @property
    def partial_args(self):
        'Partial command line options'
        if not self._args:
            self._args, _ = self.argparser.parse_known_args()
        return self._args

    @property
    def overrides(self):
        'Configuration override, stores the --with section option value'
        if not self._overrides:
            self._overrides = defaultdict(lambda: defaultdict(None))
            overrides = vars(self.partial_args).get('with')
            if overrides:
                for (s, k, v) in overrides:
                    self._overrides[s][k] = v

        return self._overrides

    def add_help(self):
        'Help parameter'
        # Taken from the constructor of argparse
        self.argparser.add_argument(
            '-h', '--help', action='help', default=argparse.SUPPRESS,
            help=argparse._('show this help message and exit'))

    def read_config_files(self):
        'Feed config_files to the cfgparser'
        if not self.config_files:
            config_files = map(
                os.path.expanduser, self.partial_args.config_files)
            self.config_files = util.OrderedSet()
            for name in config_files:
                for path, directories, filenames in os.walk(name):
                    self.config_files.extend(
                        os.path.join(path, filename)
                        for filename in filenames if filename.endswith('.conf')
                    )
                # Keep the directory names, if we write something,
                # we want to write to the last item, either a file or a new
                # file in a dir
                self.config_files.append(name)

        return self.cfgparser.read(self.config_files)

    def update(self, partial=False):
        'Force reprocessing the command line options'
        self._args = None
        if partial:
            return self.partial_args
        return self.args

    def _get_raw_inherit(self, option, raw, inherit):
        '''Sane init raw, inherit

        If the value is None then the schema is used, otherwise it returns
        what was received.
        '''
        if raw is None:
            raw = (option in self.schema and
                   self.schema[option].type in self._schema_raw_types)
        if inherit is None:
            inherit = (option not in self.schema or
                       self.schema[option].inherit)
        return raw, inherit

    def _get_maps(self, section, raw, inherit, known, instance_dict):
        '''Maps generator

        Generate the inheritance map sequence (similar to the mro in python
        classes).
        '''

        def visit(section):
            if section in self.cfgparser:
                d = dict(self.cfgparser.items(section, raw=True))
                yield d

        def queue_maps():
            # trivial queries
            yield known
            # Process overrides first
            yield self.overrides.get(section, {})
            # command line options
            yield vars(self.partial_args)

            # configurations of the section
            for mapping in visit(section):
                yield mapping
            # ask the member for internal values like the ones that need to query
            # the files
            yield instance_dict

            # ask the parents
            if inherit:
                # tsort the parents subgraph
                for parent in self._tsort_parents(section):
                    # Process overrides first
                    yield self.overrides.get(parent, {})
                    for mapping in visit(parent):
                        yield mapping

            for mapping in visit('DEFAULT'):
                yield mapping
            yield self.defaults

        return queue_maps()

    def _resolve_aliases(self, field, visited=None):
        'Follow aliases'
        if field not in self.aliases:
            return field
        if not visited:
            visited = set()
        if field in visited:
            raise ValueError(
                '{}: Invalid alias {}'.format(self.__class__.__name__, field))
        visited.add(field)
        return self._resolve_aliases(self.aliases[field], visited)

    def _chain_get(self, chain, lookup, default_value):
        '''Find lookup and it's corresponding stack of modifiers

        When querying for a list, also look for keys
        ${lookup}+ and ${lookup}- to mangle the result. This produces a stack
        of (operation, modification).
        '''

        schema = self.schema.get(lookup, Schema())
        if schema.type not in self._schema_list_types:
            return chain.get(lookup, default_value), None

        stack = []

        add_key = '{}+'.format(lookup)
        del_key = '{}-'.format(lookup)
        for mapping in chain.maps:
            try:
                if lookup in mapping:
                    value = mapping[lookup]
                    if value is not None:
                        return value, stack
                if add_key in mapping and mapping[add_key]:
                    stack.append(('+', mapping[add_key]))
                if del_key in mapping and mapping[del_key]:
                    stack.append(('-', mapping[del_key]))
            except KeyError:
                pass
        return default_value, stack

    def _process_stack(self, value, stack, option, section, raw, known,
                       instance_dict, active):
        'Process the stack, altering the value'

        def apply_op(op, values, args):
            if op == '+':
                for arg in args:
                    if arg not in values:
                        values.append(arg)
            elif op == '-':
                for arg in args:
                    if arg in values:
                        values.remove(arg)
            return values

        result = self._follow_schema(value, option, section, raw, known,
                                     instance_dict, active)
        while stack:
            op, item = stack.pop()
            expanded_item = self._follow_schema(
                item, option, section, raw, known, instance_dict, active)

            result = apply_op(op, result, expanded_item)
        return result

    def _get(self, option, active, default_value=None, section='',
             raw=None, inherit=None, known=None, instance_dict=None):
        'Retrieve the configuration option'

        if default_value is None and option in self.defaults:
            default_value = self.defaults[option]

        raw, inherit = self._get_raw_inherit(option, raw, inherit)

        maps = util.CachingIterable(
            self._get_maps(section, raw, inherit, known, instance_dict))
        chain = util.ChainMap()
        chain.maps = maps

        lookup = self._resolve_aliases(option)

        value, stack = self._chain_get(chain, lookup, default_value)

        return self._process_stack(value, stack, option, section, raw, known,
                                   instance_dict, active)

    def get(self, option, default_value=None, section='',
            raw=None, inherit=None, instance=None, **kw):
        'Interpolate values in the configuration value'
        # Initialize known values
        if default_value is None and option in self.defaults:
            default_value = self.defaults[option]
        known = util.chain_map(kw, option=option, section=section,
                               name=section, default_value=default_value)

        instance_dict = None
        if instance and hasattr(instance, '_getter'):
            instance_dict = instance._getter()
        else:
            instance_dict = {}
        # avoid loops in the interpolation
        active = OrderedDict()
        value = self._get(option, active, default_value, section=section,
                          raw=raw, inherit=inherit, known=known,
                          instance_dict=instance_dict)
        return value

    def _follow_schema(self, value, option, section, raw, known,
                       instance_dict, active):
        'Process the value as defined by it\'s schema'
        if value is None:
            return value

        schema = self.schema.get(option, Schema())
        # call the corresponding handler
        attr_name = '_get_{}'.format(schema.type)
        obj = getattr(self, attr_name)
        return obj(value, option, section, raw, known, instance_dict, active)

    def _get_rawstring(self, value, *a, **kw):
        'Rawstring are not modified'
        return value

    def _get_multiraw(self, value, *a, **kw):
        'MultiRaw split on newlines'
        if isinstance(value, str):
            value = filter(lambda x: x is not '', value.split('\n'))
        return value

    def _get_string(self, *a, **kw):
        'String get interpolated'
        return self._interpolate(*a, **kw)

    def _get_multistring(self, value, *a, **kw):
        'MultiString are interpolated MultiRaw'
        value = self._interpolate(value, *a, **kw)
        result = []
        for part in self._get_multiraw(value, *a, **kw):
            aux = self._interpolate(part, *a, **kw)
            if aux:
                result.append(aux)
        return result

    def _get_multivalue(self, value, *a, **kw):
        'MultiValue are String that get split on spaces or commas'
        value = self._interpolate(value, *a, **kw)
        if isinstance(value, str):
            value = util.split(value)
        return value

    def _get_int(self, value, *a, **kw):
        '''Int an interpolated int

        This could cause an exception to be raised
        '''
        value = self._interpolate(value, *a, **kw)
        return int(value)

    def _get_bool(self, value, *a, **kw):
        'Bool an interpolated bool, yes, on, true are all valid'
        value = self._interpolate(value, *a, **kw)
        if isinstance(value, str):
            value = util.str2bool(value)
        return value

    def _get_path(self, value, *a, **kw):
        '''Path an interpolated path

        Also, ~ is expanded to the current user's HOME, and the value drops
        useless parts as foo/../foo
        '''
        value = self._interpolate(value, *a, **kw)
        value = os.path.expanduser(value)
        value = os.path.normpath(value)
        return value

    def _get_multipath(self, value, *a, **kw):
        'MultiPath a MultiString of Path'
        xs = self._get_multistring(value, *a, **kw)
        for i, v in enumerate(xs):
            xs[i] = os.path.expanduser(v)
            xs[i] = os.path.normpath(xs[i])
        return xs

    def _interpolate(self, value, option, section, raw, known,
                     instance_dict, active):
        'Process format string values, avoid circular dependencies'

        def learn_fields(required_fields):
            if any(f in active for f in required_fields):
                raise(ValueError('Circular values dependency'))
            for field in required_fields:
                if field in known:
                    continue
                value = self._get(field, active, '', section=section,
                                  known=known, instance_dict=instance_dict)
                if isinstance(value, str):
                    _, fields = util.expansions_needed(value)
                    if fields:
                        value = interpolate(field, value, fields)
                known[field] = value

        def interpolate(option, format_string, required_fields):
            active[option] = (format_string, required_fields)
            learn_fields(required_fields)
            assert(all(f in known for f in required_fields))
            # Apply the format
            value = format_string.format(**known)
            del(active[option])
            return value

        if raw or not isinstance(value, str):
            return value
        # Interpolate the values
        n, fields = util.expansions_needed(value)
        assert(n == 0)
        if fields:
            value = interpolate(option, value, fields)
        return value

    def _init_inheritance(self):
        'Init groups, members and parents'

        if self._groups is None:
            self._groups = OrderedDict()
        if self._members is None:
            self._members = OrderedDict()
        if self._parents is None:
            self._parents = defaultdict(OrderedDict)

    @property
    def groups(self):
        'Groups, items that contain other members'
        if self._groups is None:
            self._init_inheritance()
            self.update_inheritance()

        return self._groups.keys()

    @property
    def packages(self):
        'Deprecated, use members'
        return self.members

    @property
    def members(self):
        'Configuration items'
        if self._members is None:
            self._init_inheritance()
            self.update_inheritance()

        return self._members.keys()

    @property
    def parents(self):
        'Relationships between members and groups'
        if self._parents is None:
            self._init_inheritance()
            self.update_inheritance()

        return self._parents

    def list_all(self):
        'All the configuration items'
        return list(self.groups) + list(self.members)

    def _get_parents(self, name):
        'Get the parents mappings for the item'

        if name in self._parents_cache:
            return self._parents_cache[name]

        def get_inmediate_parents(name):
            inmediate_parents = list(self.parents.get(name, {}).keys())
            # The parents are visited in reversed order.
            return reversed(tuple(inmediate_parents))

        # build a parent: {childs} map
        parents = OrderedDict()
        bfs_iter = util.bfs_gen(
            (name, None),
            get_neighbors=lambda x:
                ((p, x[0]) for p in get_inmediate_parents(x[0])),
            visit=lambda x: x)
        for parent, child in bfs_iter:
            parents.setdefault(parent, util.OrderedSet()).add(child)

        self._parents_cache[name] = parents
        return parents

    def list_parents(self, name):
        'Get the parents for the item'
        return self._get_parents(name).keys()

    def _tsort_parents(self, name):
        '''Use the parents mappings to set the parents resolution order

        We are using a topological sort, the idea here is that a inherits from
        general_group and specific_group, while specific_group also inherits
        form general_group. We want to query a, then specific_group, then
        general_group.
        '''
        # This could be cached
        parents = self._get_parents(name)
        tsort = TSortGraph(parents.keys(), get_inputs=lambda x: parents[x])
        return tsort.sort_generator()

    def get_packages(self, name):
        'Deprecated, use get_members'
        return self.get_members(name)

    def get_members(self, name):
        'Expand item name to a sequence of leaf members'

        def expand(name, expanded):
            expanded.add(name)

            names = util.OrderedSet()
            for member in self._groups.get(name, {}):
                if member in self.members:
                    names.add(member)
                elif member in expanded:
                    continue
                names.extend(expand(member, expanded))
            return names

        ok, members = self._ensure_item(name)
        # logging.debug('name %s ensure returned %s, %s', name, ok, members)
        if not ok:
            return
        if members is None:
            return util.OrderedSet([name])
        return expand(name, set())

    def _ensure_item(self, name):
        '''Make sure item is present

        Also handle suite of the form "member/suite", which is a quick way to
        use member inheriting from suite.
        '''
        if name is None:
            return None, None
        if name in self._members:
            return name, None
        if name in self._groups:
            return name, self._groups[name]
        # logging.debug('name %s not in _members or _groups', name)
        basename, suite = util.split_basename_suite(name)
        logging.debug('name %s base %s suite %s', name, basename, suite)
        ok, _ = self._ensure_item(suite)
        if not ok:
            return None, None
        logging.debug('suite %s suite_ok %s', suite, ok)
        ok, members = self._ensure_item(basename)
        logging.debug('base %s base_ok %s members %s', basename, ok, members)
        if not ok:
            return None, None
        if members is not None:
            members = [
                util.join_basename_suite(member, suite) for member in members
            ]
        self._add_item(name, members=members, inherits=(suite, basename))
        return name, members

    def _add_item(self, name, members=None, inherits=None):
        'Add item "name", with members and inherits'
        self.add_section(name)
        if members is not None:
            self._groups[name] = members
            for member in members:
                self._ensure_item(member)
                self._parents[member][name] = True
            self.set(name, 'members', util.join(members))
        else:
            self._members[name] = None
        for parent in reversed(inherits or []):
            self._parents[name][parent] = True
        self.set(name, 'inherits', util.join(inherits))

    def update_inheritance(self):
        'Update inheritance'

        sections = self.cfgparser.sections()
        sections.extend(self.overrides.keys())

        for section in sections:
            if section == 'DEFAULT':
                # The cost of handling the default ourselves
                continue
            # The inherit=False is not really needed, as it's in the schema,
            # it's here as a reminder.
            members = self.get('members', section=section, inherit=False)
            # This is deprecated, use members instead
            section_packages = self.get('packages', section=section,
                                        inherit=False)
            # If it contains no members it's a package
            if section_packages is None and members is None:
                self._members[section] = None
            else:
                # If it contains members it's a group
                self._groups.setdefault(section, [])
                self._groups[section] += members or []
                self._groups[section] += section_packages or []

            # Add relationship between the item and the group
            for member in self._groups.get(section, {}):
                self._parents[member][section] = True

        all_items = self._groups.keys() | self._members.keys()
        all_group_members = set(itertools.chain.from_iterable(
            self._groups.values()))
        missing_members = all_group_members - all_items
        # groups can define implicit members, add those too
        for member in missing_members:
            self._members[member] = None
            all_items.add(member)

        # Process inherits
        for item in all_items:
            inherits = self.get('inherits', section=item, inherit=False)
            # The parents are visited in reversed order.
            # Keep inherits in the order entered in the configuration.
            for parent in reversed(inherits or []):
                # Avoid adding the relation with a non existing parent
                if item in all_items:
                    self._parents[item][parent] = True

    def write(self, section_name):
        '''Update the section in the config files'''
        config_file = self.config_files[-1]

        if os.path.isdir(config_file):
            config_file = os.path.join(
                config_file,
                '{}.conf'.format(section_name))
        if os.path.exists(config_file):
            backup_file = config_file + '.bak'
            shutil.copyfile(config_file, backup_file)
        # We are not keeping the kwargs used in the constructior, maybe we
        # should
        cfgparser = configparser.ConfigParser(default_section='')
        cfgparser.read(config_file)

        # Copy the values from the current config
        for option, value in dict(
            self.cfgparser.items(section_name, raw=True) or
                {}):
            cfgparser.set(section_name, option, value)
        # Write the section overrides
        for option, value in (self.overrides[section_name] or {}).items():
            if value is None:
                continue
            cfgparser.set(section_name, option, value)

        with open(config_file, 'w') as f:
            self.cfgparser.write(f)

    def arg_add(self, *args, **kwargs):
        'Add a command line option'
        return self.argparser.add_argument(*args, **kwargs)

    def add_section(self, *args, **kwargs):
        'Add a config parser section'
        return self.cfgparser.add_section(*args, **kwargs)

    def set(self, section, option, value):
        'Add an override'
        self.overrides[section][option] = value
        return value


def main():
    config = Configuration()
    print(config.partial_args)
    config.read_config_files()
    print(config.get('test'))
    print('{} {}'.format(config.get('test', section='foo')))
    print(config.members)


if __name__ == '__main__':
    main()
