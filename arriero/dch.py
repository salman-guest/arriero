#!/usr/bin/env python3
# -*- coding: utf8 -*-
# Copyright: 2017, Maximiliano Curia <maxy@debian.org>
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

import logging
import os

from . import util
from .constants import ERROR, OK
from .version import Version


class Dch(object):

    @staticmethod
    def commit(package, msg, files):
        cmd = ['dch', '--', msg]
        util.log_call(cmd, cwd=package.path)
        package.git.add(os.path.join('debian', 'changelog'))
        package.git.add(*files)
        package.git.commit('-m', msg)

    @staticmethod
    def new_version(package, msg, version):
        cmd = ['dch', '-p', '-v', str(version), '--', msg]
        util.log_call(cmd, cwd=package.path)

        package.git.add(os.path.join('debian', 'changelog'))
        package.git.commit('-m', msg)

    @staticmethod
    def pre_release(package, unreleased, ignore_branch):

        if not (package.version.is_pre_release() or unreleased):
            msg = 'New revision'
            if package.version.vendor and not package.version.vendor_revision:
                # Fix up version
                version = '{}0'.format(package.version)
                dch_cmd = ['dch', '-p', '-v', str(version), '--', msg]
                util.log_call(dch_cmd, cwd=package.path)
                msg = ''
            dch_cmd = ['dch', '-i']
            dch_cmd += ['--', msg]
            util.log_call(dch_cmd, cwd=package.path)
            package.update_changelog()

        dch_cmd = ['dch', '-b']
        new_version = package.version.pre_release()
        dch_cmd += ['-v', str(new_version), '--', '']
        util.log_call(dch_cmd, cwd=package.path)

        msg = 'Pre-release {}'.format(new_version)
        package.git.add(os.path.join('debian', 'changelog'))
        package.git.commit('-m', msg)
        return OK

    @staticmethod
    def release(package, distribution, unreleased, ignore_branch):

        cmd = ['dch']
        msg = ''

        version = package.version

        if unreleased:
            if version.is_pre_release():
                version = version.release()
                dch_cmd = ['dch', '--release-heuristic', 'changelog',
                           '-v', str(version), '--', '']
                util.log_call(dch_cmd, cwd=package.path)
            cmd.append('-r')
        else:
            if distribution:
                if package.distribution == distribution:
                    logging.error('%s: Already released for %s',
                                  package.name, distribution)
                    return ERROR
                msg = 'Release to {}'.format(distribution)
                cmd.append('-i')

        if distribution and distribution.lower() != 'unreleased':
            cmd += ['-D', distribution]

        cmd += ['--', msg]
        util.log_call(cmd, cwd=package.path)

        package.update_changelog()

        msg = 'Release to {}'.format(package.distribution)
        package.git.add(os.path.join('debian', 'changelog'))
        package.git.commit('-m', msg)

        return OK


class GbpDch(object):

    @staticmethod
    def commit(package, msg, files, allow_empty=False):
        package.git.add(*files)
        args = ['-m', msg]
        if allow_empty:
            args.append('--allow-empty')
        package.git.commit(*args)

    @staticmethod
    def new_version(package, msg, version):
        GbpDch.commit(package, msg, tuple(), allow_empty=True)

        cmd = ['gbp', 'dch', '-a', '--verbose',
               '--debian-branch={}'.format(package.debian_branch),
               '-N', str(version),
               '--multimaint',
               '--multimaint-merge',
               '--git-log=--not {} --no-merges'.format(
                   package.tag_template('upstream', Version(version).upstream_version)
               ),
               '--commit']

        util.log_call(cmd, cwd=package.path)

    @staticmethod
    def pre_release(package, unreleased, ignore_branch):

        cmd = ['gbp', 'dch', '-a',
               '--multimaint',
               '--multimaint-merge',
               '--commit',
               '--spawn-editor=never']

        bpo = package.get('bpo')

        if (not (package.version.is_pre_release() or unreleased) or
                bpo and not package.version.is_backport()):
            msg = '' if bpo else 'New revision'
            if package.version.vendor and not package.version.vendor_revision:
                # Fix up version
                version = '{}0'.format(package.version)
                dch_cmd = ['dch', '-p', '-v', str(version), '--', msg]
                util.log_call(dch_cmd, cwd=package.path)
                msg = ''
            dch_cmd = ['dch']
            dch_cmd.append('--bpo' if bpo else '-i')
            if bpo:
                dch_cmd += ['-D', 'UNRELEASED']
            dch_cmd += ['--', msg]
            util.log_call(dch_cmd, cwd=package.path)
            package.update_changelog()
            unreleased = True

        new_version = package.version.pre_release()

        if unreleased:
            dch_cmd = ['dch', '-b', '-v', str(new_version), '--', '']
            util.log_call(dch_cmd, cwd=package.path)
            package.update_changelog()

        else:
            cmd += ['-N', str(new_version)]

        if ignore_branch:
            cmd.append('--ignore-branch')
        else:
            cmd.append('--debian-branch={}'.format(
                package.debian_branch))

        util.log_call(cmd, cwd=package.path)
        return OK

    @staticmethod
    def release(package, distribution, unreleased, ignore_branch):

        if distribution and package.distribution == distribution:
            logging.error('%s: Already released for %s',
                          package.name, distribution)
            return ERROR

        cmd = ['gbp', 'dch', '-a',
               '--multimaint',
               '--multimaint-merge',
               '-R',
               '--commit',
               '--spawn-editor=never']

        if ignore_branch:
            cmd.append('--ignore_branch')
        else:
            cmd.append('--debian-branch={}'.format(
                package.debian_branch))

        if distribution:
            cmd += ['-D', distribution]

        if package.version.is_pre_release():
            new_version = package.version.release()
            cmd += ['-N', str(new_version)]

        bpo = package.get('bpo')
        if bpo:
            cmd.append('--bpo')

        if distribution:
            msg = 'Release to {}'.format(distribution)
            GbpDch.commit(package, msg, tuple(), allow_empty=True)

        util.log_call(cmd, cwd=package.path)
        return OK


DEFAULT_DCH = 'dch'
AVAILABLE_DCH = {
    'gbp-dch':   (GbpDch, 'Use gbp dch'),
    DEFAULT_DCH: (Dch,    'Use dch'),
}


def get_dch(config):
    name = config.get('dch')
    if name not in AVAILABLE_DCH:
        name = DEFAULT_DCH
    return AVAILABLE_DCH[name][0]
